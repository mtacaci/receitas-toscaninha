# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include toscaninha_cliente
class toscaninha_cliente {
group { 'java':
       ensure => 'present',
       gid    => '501',
     }
user { 'java':
       ensure           => 'present',
       gid              => '501',
       home             => '/home/java',
       password         => '!!',
       password_max_age => '99999',
       password_min_age => '0',
       shell            => '/bin/bash',
       uid              => '501',
     }
file { '/opt/apps':
        ensure => directory,
        owner => 'java',
        group => 'java',
    }
file { '/opt/apps/cliente':
        ensure => directory,
        owner => 'java',
        group => 'java',
    }
$content = "Versao 1.0"
  file { '/opt/apps/cliente/versao.txt':
    ensure  => file,
    content => $content,
    mode    => '0644',
    owner => 'java',
    group => 'java',
  }
file { "/etc/systemd/system/cliente.service": 
        mode => "0644",
        owner => 'root',
        group => 'root',
        source => 'puppet:///modules/toscaninha_cliente/cliente.service',
     }
package { 'maven':
        ensure => installed,
        name   => $maven,
    }

remote_file { '/opt/apps/cliente/cliente.jar':
    ensure => latest,
    owner => 'java',
    group => 'java',
    source => 'http://23.96.48.125/artfactory/toscaninha-cliente/cliente.jar',
  }

}
