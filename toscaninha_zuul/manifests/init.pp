# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include toscaninha_zuul
class toscaninha_zuul {
file { '/opt/apps/toscaninha-zuul':
ensure => directory,
owner => 'java',
group => 'java',
}
file { "/etc/systemd/system/toscaninha-zuul.service":
mode => "0644",
owner => 'root',
group => 'root',
source => 'puppet:///modules/toscaninha_zuul/zuul.service',}
file { "/opt/apps/toscaninha-zuul/application.properties":
mode => "0644",
owner => 'java',
group => 'java',
source => 'puppet:///modules/toscaninha_zuul/bootstrap.txt',
}
remote_file { '/opt/apps/toscaninha-zuul/zuul.jar':
ensure => latest,
owner => 'java',
group => 'java',
source => 'http://23.96.48.125/artfactory/zuul/zuul.jar',
}
}
