server.port=8083
spring.application.name=monitor
eureka.client.registerWithEureka=true
eureka.client.serviceUrl.defaultZone=${EUREKA_URI:http://23.96.116.31:8080/eureka}
