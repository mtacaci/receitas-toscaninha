# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include toscaninha_produto
class toscaninha_produto {
group { 'java':
       ensure => 'present',
       gid    => '501',
     }
user { 'java':
       ensure           => 'present',
       gid              => '501',
       home             => '/home/java',
       password         => '!!',
       password_max_age => '99999',
       password_min_age => '0',
       shell            => '/bin/bash',
       uid              => '501',
     }
exec { 'refresh_toscaninha_produto':
command => "/usr/bin/systemctl restart toscaninha-produto",
refreshonly => true,
}
exec { 'refresh_daemon':
command => "/usr/bin/systemctl daemon-reload",
refreshonly => true,
}
file { '/opt/apps':
ensure => directory,
owner => 'java',
group => 'java',
}
file { '/opt/apps/toscaninha-produto':
ensure => directory,
owner => 'java',
group => 'java',
}
file { "/etc/systemd/system/toscaninha-produto.service":
mode => "0644",
owner => 'root',
group => 'root',
source => 'puppet:///modules/toscaninha_produto/toscaninha-produto.service',
notify => Exec['refresh_daemon'],
}
file { "/opt/apps/toscaninha-produto/application.properties":
mode => "0644",
owner => 'java',
group => 'java',
source => 'puppet:///modules/toscaninha_produto/bootstrap.txt',
notify => Exec['refresh_toscaninha_produto'],
}
package { 'maven':
        ensure => installed,
        name   => $maven,
    }
remote_file { '/opt/apps/toscaninha-produto/toscaninha-produto.jar':
ensure => latest,
owner => 'java',
group => 'java',
source => 'http://23.96.48.125/artfactory/toscaninha-produto/produto.jar',
notify => Exec['refresh_toscaninha_produto'],
}
}
